<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container" align="center">
        <h2>Inventory Management</h2>
            <div class="header">
                <h4><a href="{{url('/')}}"> <-Back Home</a></h4>
                    <h4><a href="#"> Sales Product</a></h4>
        <h3>
            @if (Session::has('msg'))
            <div class="alert alert-success center-block">{{ Session::get('msg') }}</div>
            @endif
        </h3>
                <form action="{{url('/sales')}}" method="post">
                    {{ csrf_field()}}
                    <span>Name</span>
                    <select name="product_name">
                        @foreach($data as $data)
                        <option>{{$data->name}}</option>
                        @endforeach
                    </select><br><br>
                    <span>Brand</span>
                    <select name="brand">
                        <option>hp</option>
                        <option>adata</option>
                        <option>samsung</option>
                    </select>
                    <br><br>
                    <span>Quantity</span>
                    <input type="text" name="quantity"><br><br>
                    <span>Price</span>
                    <input type="text" name="price"><br><br>

                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    </body>
</html>
