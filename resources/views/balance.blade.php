<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container" align="center">
        <h2>Inventory Management</h2>
            <div class="header">
                <h4><a href="{{url('/')}}"> <-Back Home</a></h4>
                    <h3> Store Balance</h3>
                    <table border="2" class="table table-striped" width="50%">
                        <tr>
                            <td><b>Current Store Balance</b></td>
                            
                            <td><b> Tk. {{ $cs_data }}</b></td>
                            
                        </tr>
                        <tr>
                            <td><b>Total Sales Balance</b></td>
                            <td><b>Tk. {{ $sl_data }}</b></td>
                        </tr>
                        <tr>
                            <td><b>Total Balance</b></td>
                            <td><b>Tk. {{ $to_data }}</b></td>
                        </tr>
                    </table>
            </div>
        </div>
    </body>
</html>
