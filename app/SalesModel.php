<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesModel extends Model
{
    protected $table='sales';
   protected $fillable=['name','brand','quantity','price'];
}
