<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\SalesModel;
use DB;

class CheckController extends Controller
{
    public function CheckStock(){
    	$data = ProductModel::all();
    	return view('stock', ['data'=>$data]);
    }

    public function CheckBalance(){
    	
		$cs_result=DB::table('product')->sum('price');
		$sl_result=DB::table('sales')->sum('price');
		$total_result=$cs_result + $sl_result;
    	return view('balance')->with('cs_data',$cs_result)
    						  ->with('sl_data',$sl_result)
    						  ->with('to_data',$total_result); 

    }
}
