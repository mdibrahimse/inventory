<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\SalesModel;
use DB;
class SalesController extends Controller
{
    public function index(){
    	$data = ProductModel::all();

    	return view('sales', ['data'=>$data]);
    }
    public function SalesProduct(Request $request){
    	
    	$data = new SalesModel();
    	$data->name=$request->product_name;
    	$data->brand=$request->brand;
    	$data->quantity=$request->quantity;
    	$data->price=$request->price;
    	$data->save();
    	
	
    	//checking stock product
    	$check = DB::table('product')->where([
        ['name','=',$request->product_name],
        ['brand','=',$request->brand]
        ])->get();
        if (count($check)==0) {
        	echo "No Product in Stock !!";
        }else{
   		//update stock
    	$data = new ProductModel();
    	$data=ProductModel::where('name', '=', $request->product_name)->first();
    	$data->quantity=$data->quantity - $request->quantity;
    	$data->price=$data->price - $request->price;
    	$data->save();
    
    	return redirect('/sales-product')->with('msg', 'Product Sales successfully.');

    	}
    }






}
