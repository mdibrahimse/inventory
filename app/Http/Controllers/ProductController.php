<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
class ProductController extends Controller
{
    public function index(){
    	return view('add_product');
    }
    public function getProduct(Request $request){
    	$data = new ProductModel();
    	$data->name=$request->product_name;
    	$data->brand=$request->brand;
    	$data->quantity=$request->quantity;
    	$data->price=$request->price;
    	$data->save();
    	return redirect('/add-product')->with('msg', 'Product added successfully.');
    }

 






}
