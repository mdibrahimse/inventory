<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/add-product','ProductController@index');
Route::post('/get-product','ProductController@getProduct');
Route::get('/sales-product','SalesController@index');
Route::post('/sales','SalesController@SalesProduct');
Route::get('/current-stock','CheckController@CheckStock');
Route::get('/stock-balance','CheckController@CheckBalance');